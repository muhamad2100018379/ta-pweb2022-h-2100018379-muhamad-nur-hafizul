<?php 
$arrnilai=array("Adit"=>88,"Fadil"=>99,"fatimah"=>86,"Galang"=>75);
echo "<b>Array sebelum di urutkan </b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

sort($arrnilai);
reset($arrnilai);
echo "<b> Array setelah diurutkan dengan asort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

arsort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan arsort</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";
?>